import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Plugin from './pages/Plugin';

// CRIA AS ROTAS DA APLICAÇÃO E DESIGNA SUAS RESPECTIVAS PAGINAS
export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                {/* EXEMPLO: ROTA "/kms" RENDERIZA A PAGINA "Kms" */}
                <Route path="/" exact component={Plugin} />
            </Switch>
        </BrowserRouter>
    );
}
