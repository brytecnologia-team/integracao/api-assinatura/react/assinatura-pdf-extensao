import React from 'react';

import './App.css';

import logo from './assets/logobry.png';

import Routes from './routes';

function App() {
// CRIA HTML BASE DE TODA APLICAÇÃO
  return (
    <div className="container">
      <img className="logo" src={logo} alt="BRy Tecnologia" />
      <div className="content">
        <Routes />
      </div>
    </div>
  );
}

export default App;
